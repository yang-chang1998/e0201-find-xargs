import os
import random
import shutil
import datetime
from pathlib import Path


dirbase = Path("files2")
shutil.rmtree(dirbase, ignore_errors=True)
chars = "efg"
fakedt = datetime.datetime(2019, 9, 1).timestamp()


for c in chars:
    dirpath = dirbase / (c * 4)
    dirpath.mkdir(parents=True, exist_ok=True)
    for i in range(50):
        filepath = dirpath / f"'{i:02}' \"{i:02}\".txt"
        content = "ok" if random.random() < 0.2 else "important"
        filepath.write_text(content)

        if random.random() > 0.9:
            os.utime(filepath, (fakedt, fakedt))


print("files2 created.")
