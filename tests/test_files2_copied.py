from pathlib import Path


source = Path("files2")
target = Path("files2-selected")


def test_target_exists():
    """
    检查目标文件夹被创建
    """
    assert target.is_dir()


def test_copied_by_size():
    """
    检查 9 字节大小的文件都已被复制
    """
    for f in source.glob("**/*"):
        if f.is_file() and f.stat().st_size == 9:
            assert (target.joinpath(*f.parts[1:])).exists()


def test_number_of_files():
    """
    检查复制的文件数占比约为 0.8
    """
    num_source = sum(1 for p in source.glob("**/*"))
    num_target = sum(1 for p in target.glob("**/*"))
    ratio = num_target / num_source
    assert 0.7 < ratio < 0.9


def test_target_file_content():
    """
    检查复制的文件内容均为 "important"
    """
    entries = list(target.glob("**/*"))
    assert entries
    for p in entries:
        if p.is_dir():
            continue
        assert p.read_text() == "important"


def test_copied_in_source():
    """
    检查目标文件夹 files2-selected 中的文件，均存在于 files2 文件夹中，没有多余
    """
    entries = list(target.glob("**/*"))
    assert entries
    for f in entries:
        if not f.is_file():
            continue
        s = source.joinpath(*f.parts[1:])
        assert s.exists()
        assert s.read_text() == f.read_text()
